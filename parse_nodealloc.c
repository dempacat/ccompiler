#include "compiler.h"
#include "parse_internal.h"

#include <stdlib.h>

Node* new_node(int ty, Node *lhs, Node *rhs)
{
    Node *node = malloc(sizeof(Node));
    node->ty = ty;
    node->lhs = lhs;
    node->rhs = rhs;
    node->anly_flag = 0;
    return node;
}

Node *new_node_num(int val)
{
    Node *node = malloc(sizeof(Node));
    node->ty = NUM;
    node->val = val;
    node->anly_flag = 0;
    return node;
}

Node *new_node_ident(const char *name)
{
    Node *node = malloc(sizeof(Node));
    node->ty = SYM;
    node->ident = name;
    node->anly_flag = 0;
    return node;
}

Node *new_node_compound(int ty, Vec v)
{
    Node *node = malloc(sizeof(Node));
    node->ty = ty;
    node->children = v;
    node->anly_flag = 0;
    return node;
}

Node *new_node_conditional(int ty, Node *cond, Node *lhs, Node *rhs)
{
    Node *node = malloc(sizeof(Node));
    node->ty = ty;
    node->cond = cond;
    node->lhs = lhs;
    node->rhs = rhs;
    node->anly_flag = 0;
    return node;
}

Node *new_node_function(const char *name)
{
    Node *node = malloc(sizeof(Node));
    node->ty = FUNCTION;
    node->env = allocate_env();
    node->ident = name;
    node->anly_flag = 0;
    return node;
}

Node *copy_node(const Node *node)
{
    Node *newnode = malloc(sizeof(Node));
    *newnode = *node;
    return newnode;
}
