#include "compiler.h"
#include "parse_internal.h"

static Node *parse_stmts(const Token *tokens, int *pos, Env *env, int ty);

static Node *parse_declvar(const Token *tokens, int *pos, Env *env)
{
    Node *assign = 0, **tail = &assign;
    int declared = 0;
    if (tokens[*pos].ty != INT) {
        eprintf("parse_declvar: Not a type\n");
        return 0;
    }
    while (1) {
        Type ty = {Int, 0};
        (*pos)++;
        while (tokens[*pos].ty == '*') {
            Type tmp = ty;
            ty.ty = Ptr;
            ty.ptrof = malloc(sizeof(ty));
            *ty.ptrof = tmp;
            (*pos)++;
        }
        if (tokens[*pos].ty != SYM) {
            eprintf("parse_declvar: Syntax Error. identifier shall be specified.\n");
            return 0;
        }
        char *sym = tokens[*pos].sym;
        (*pos)++;
        while (tokens[*pos].ty == '[') {
            (*pos)++;
            // TODO: support `int a[] = {...};`
            int arrsize;
            if (tokens[*pos].ty == NUM) {
                arrsize = tokens[*pos].val;
            } else {
                eprintf("parse_declvar: Syntax Error. Size of array is expected.\n");
                return 0;
            }
            (*pos)++;
            if (tokens[*pos].ty != ']') {
                eprintf("parse_declvar: Syntax Error. `]` is expected.\n");
                return 0;
            }
            (*pos)++;
            Type tmp = ty;
            ty.ty = Array;
            ty.arrsize = arrsize;
            ty.ptrof = malloc(sizeof(ty));
            *ty.ptrof = tmp;
        }
        // TODO: move to analyze
        decl_variable(env, sym, ty, Local);
        declared++;
        if (tokens[*pos].ty == '=') {
            (*pos)++;
            // TODO: Array initializer
            Node *expr = new_node('=', new_node_ident(sym), parse_assign(tokens, pos, env));
            if (!assign) {
                assign = expr;
            } else {
                *tail = new_node(',', *tail, expr);
                tail = &(*tail)->rhs;
            }
        }
        if (tokens[*pos].ty != ',') break;
    }
    return new_node(DECLVAR, assign, 0);
}

static Node *parse_stmt(const Token *tokens, int *pos, Env *env)
{
    Node *node = 0;
    if (tokens[*pos].ty == RETURN) {
        (*pos)++;
        node = new_node(RETURN, parse_expr(tokens, pos, env), 0);
    } else if (tokens[*pos].ty == IF){
        Node *cond, *lhs, *rhs = 0;
        if (tokens[*pos + 1].ty != '(') {
            eprintf("Strange if statement (No open bracket in condition)\n");
            return 0;
        }
        (*pos) += 2;
        cond = parse_expr(tokens, pos, env);
        if (tokens[*pos].ty != ')') {
            eprintf("Strange if statement (No close bracket in condition)\n");
            return 0;
        }
        (*pos)++;
        lhs = parse_stmts(tokens, pos, env, COMPOUND_STMTS);
        if (tokens[*pos].ty == ELSE) {
            (*pos)++;
            rhs = parse_stmts(tokens, pos, env, COMPOUND_STMTS);
        }
        return new_node_conditional(IF, cond, lhs, rhs);
    } else if (tokens[*pos].ty == INT) {
        node = parse_declvar(tokens, pos, env);
    } else if (0) {
    } else if (tokens[*pos ].ty == WHILE) {
        Node *cond, *lhs;
        if (tokens[*pos + 1].ty != '(') {
            eprintf("Strange while statement (No open bracket in condition)\n");
            return 0;
        }
        (*pos) += 2;
        cond = parse_expr(tokens, pos, env);
        if (tokens[*pos].ty != ')') {
            eprintf("Strange while statement (No close bracket in condition)\n");
            return 0;
        }
        (*pos)++;
        lhs = parse_stmts(tokens, pos, env, COMPOUND_STMTS);
        return new_node_conditional(WHILE, cond, lhs, 0);
    } else if (0) {
    } else if (0) {
    } else if (0) {
    } else {
        node = parse_expr(tokens, pos, env);
    }
    if (tokens[*pos].ty == ';') {
        (*pos)++;
        return node;
    } else {
        if (node) {
            eprintf("parse_stmt: node: %d ", node->ty);
        } else {
            eprintf("parse_stmt: node: null ");
        }
        eprintf("セミコロンないよ\n");
        return 0;
    }
}

static Node *parse_stmts(const Token *tokens, int *pos, Env *env, int ty)
{
    Vec vec = vec_new(sizeof(Node));
    if (tokens[*pos].ty != '{') {
        vec_free(&vec);
        return parse_stmt(tokens, pos, env);
    }
    (*pos)++;
    while (tokens[*pos].ty != '}') {
        if (tokens[*pos].ty == FILE_END) {
            eprintf("Syntax Error: Unclosed bracket\n");
            vec_free(&vec);
            return 0;
        }
        vec_push(&vec, parse_stmt(tokens, pos, env));
    }
    (*pos)++;
    return new_node_compound(ty, vec);
}

static Node *parse_program(const Token *tokens, int *pos)
{
    Vec v = vec_new(sizeof(Node));
    // Generally, it may a decl of global function. In this case, ignore it.
    // And, `foo()` is as same as `int foo()`. I ignored it now.
    while (tokens[*pos].ty == INT && tokens[*pos + 1].ty == SYM && tokens[*pos + 2].ty == '(') {
        const char *funcname = tokens[*pos + 1].sym;
        Env *env = allocate_env();
        (*pos) += 3;
        if (tokens[*pos].ty == VOID) {
            if (tokens[*pos + 1].ty == ')') {
                (*pos) += 2;
            } else {
                eprintf("Strange arguments in function `%s` declare.\n", funcname);
            }
        } else {
            while (tokens[*pos].ty != ')') {
                if (tokens[*pos].ty != INT) {
                    eprintf("Only int type is available.\n");
                    return 0;
                }
                (*pos)++;
                Type ty = {Int, 0};
                while (tokens[*pos].ty == '*') {
                    Type tmp = ty;
                    ty.ty = Ptr;
                    ty.ptrof = malloc(sizeof ty);
                    *(ty.ptrof) = tmp;
                    (*pos)++;
                }
                if (tokens[*pos].ty != SYM) {
                    eprintf("Argument shall be an symbol. `%s` declare.\n", funcname);
                    return 0;
                }
                decl_variable(env, tokens[*pos].sym, ty, Arg);
                (*pos)++;
                if (tokens[*pos].ty == ',') (*pos)++;
            }
            (*pos)++;
        }
        Node *node = parse_stmts(tokens, pos, env, FUNCTION);
        node->ident = funcname;
        node->env = env;
        vec_push(&v, node);
    }
    return new_node_compound(GLOBAL, v);
}

Node *parse(const Token *tokens)
{
    int pos = 0;
    // Test case still contains non-full codes for example only expression or block.
    // This is for non-full codes.
    if (tokens->ty != INT) {
        Env *env = allocate_env();
        Node *node = parse_stmts(tokens, &pos, env, FUNCTION);
        node->ident = "main";  // Now, assume main function.
        node->env = env;
        if (tokens[pos].ty != FILE_END) {
            fprintf(stderr, "Unconsumed token is still remained.\n");
        }
        Vec v = vec_new(sizeof(Node));
        vec_push(&v, node);
        return new_node_compound(GLOBAL, v);
    }
    return parse_program(tokens, &pos);
}
