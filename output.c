#include "compiler.h"

// All of these registers are belong to called function.
static const char* regs[] = {"rsi", "rdi", "r8", "r9", "r10", "r11", 0};

static const int N_ARGREGS = 6;
static const char* argregs[] = {"rdi", "rsi", "rdx", "rcx", "r8", "r9", 0};

static int output_expr_internal(const Node *node, Env *env, int idx);

static void make_label(Label label, const void *p)
{
    sprintf(label, "L%llx", (unsigned long long)p);
}

static int output_expr_assign(const Node *node, Env *env, int idx)
{
    printf("  // assignment\n");
    output_expr_internal(node->rhs, env, idx);
    output_expr_internal(node->lhs, env, idx + 1);
    printf("  mov [%s], %s\n", regs[idx + 1], regs[idx]);
    return 0;
}

static int output_expr_binop(const Node *node, Env *env, int idx)
{
    if (node->ty == '=') {
        if (output_expr_assign(node, env, idx)) return 1;
    } else {
        if (output_expr_internal(node->lhs, env, idx)) return 1;
        if (output_expr_internal(node->rhs, env, idx + 1)) return 1;
        if (node->ty == '+') {
            if (node->anly_flag & PTR_ADDSUB) {
                printf("  // pointer add\n");
                printf("  lea %s, [%s + %s * 8]\n", regs[idx], regs[idx], regs[idx + 1]);
            } else {
                printf("  // add\n");
                printf("  add %s, %s\n", regs[idx], regs[idx + 1]);
            }
        } else if (node->ty == '-') {
            if (node->anly_flag & PTR_ADDSUB) {
                printf("  // pointer sub\n");
                printf("  lea %s, [%s - %s * 8]\n", regs[idx], regs[idx], regs[idx + 1]);
            } else if (node->anly_flag & PTR_DIFF) {
                printf("  // pointer diff\n");
                printf("  sub %s, %s\n", regs[idx], regs[idx + 1]);
                printf("  sar %s, 3\n", regs[idx]);
            } else {
                printf("  // sub\n");
                printf("  sub %s, %s\n", regs[idx], regs[idx + 1]);
            }
        } else if (node->ty == '*') {
            printf("  // mul\n");
            printf("  mov rax, %s\n", regs[idx]);
            printf("  mul %s\n", regs[idx + 1]);
            printf("  mov %s, rax\n", regs[idx]);
        } else if (node->ty == '/') {
            printf("  // div\n");
            printf("  mov rax, %s\n", regs[idx]);
            printf("  mov rdx, 0\n");
            printf("  div %s\n", regs[idx + 1]);
            printf("  mov e%s, eax\n", regs[idx]+1);
        } else if (node->ty == '%') {
            printf("  // mod\n");
            printf("  mov rax, %s\n", regs[idx]);
            printf("  mov rdx, 0\n");
            printf("  div %s\n", regs[idx + 1]);
            printf("  mov e%s, edx\n", regs[idx]+1);
        } else if (node->ty == LSHIFT) {
            printf("  // lshift\n");
            printf("  mov rax, %s\n", regs[idx]);
            printf("  mov rcx, %s\n", regs[idx + 1]);
            printf("  shl %s, cl\n", regs[idx]);
        } else if (node->ty == RSHIFT) {
            printf("  // rshift\n");
            printf("  mov rax, %s\n", regs[idx]);
            printf("  mov rcx, %s\n", regs[idx + 1]);
            printf("  shr %s, cl\n", regs[idx]);
        } else if (node->ty == '<') {
            printf("  // less than\n");
            printf("  cmp %s, %s\n", regs[idx], regs[idx + 1]);
            printf("  setl al\n");
            printf("  mov %s, rax\n", regs[idx]);
        } else if (node->ty == '>') {
            printf("  // greater than\n");
            printf("  cmp %s, %s\n", regs[idx], regs[idx + 1]);
            printf("  setg al\n");
            printf("  mov %s, rax\n", regs[idx]);
        } else if (node->ty == LE) {
            printf("  // less than or equal\n");
            printf("  cmp %s, %s\n", regs[idx], regs[idx + 1]);
            printf("  setle al\n");
            printf("  mov %s, rax\n", regs[idx]);
        } else if (node->ty == GE) {
            printf("  // greater than or equal\n");
            printf("  cmp %s, %s\n", regs[idx], regs[idx + 1]);
            printf("  setge al\n");
            printf("  mov %s, rax\n", regs[idx]);
        } else if (node->ty == EQ) {
            printf("  // equal (==)\n");
            printf("  cmp %s, %s\n", regs[idx], regs[idx + 1]);
            printf("  sete al\n");
            printf("  mov %s, rax\n", regs[idx]);
        } else if (node->ty == NE) {
            printf("  // not equal (!=)\n");
            printf("  cmp %s, %s\n", regs[idx], regs[idx + 1]);
            printf("  setne al\n");
            printf("  mov %s, rax\n", regs[idx]);
        } else if (node->ty == ',') {
            output_expr_internal(node->lhs, env, idx);
            output_expr_internal(node->rhs, env, idx);
        } else if (0) {
        } else if (0) {
        } else if (0) {
        } else if (0) {
        } else if (0) {
        } else if (0) {
        } else {
            if (node->ty > 128) {
                eprintf("ごめん、未実装(binop) %d\n", node->ty);
            } else {
                eprintf("ごめん、未実装(binop) %c\n", node->ty);
            }
        }
    }
    return 0;
}

static int output_expr_internal(const Node *node, Env *env, int idx)
{
    if (!regs[idx]) {
        fprintf(stderr, "Register spill!\n");
        return 1;
    }
    if (is_binary_op(node->ty)) {
        if (output_expr_binop(node, env, idx)) return 1;
    } else if (node->ty == NUM) {
        printf("  mov %s, %d\n", regs[idx], node->val);
    } else if (node->ty == SYM) {
        const Variable *var = get_variable(env, node->ident);
        if (!var) {
            eprintf("output_expr_internal: Undeclared variable: %s\n", node->ident);
            return 1;
        }
        if (node->anly_flag & LVALUE) {
            // Load address as lvalue
            printf("  // load addr of %s\n", node->ident);
            printf("  lea %s, [rbp - %d]\n", regs[idx], var->offset);
        } else {
            // Load value as rvalue
            printf("  // load %s\n", node->ident);
            printf("  mov %s, [rbp - %d]\n", regs[idx], var->offset);
        }
    } else if (node->ty == CALL) {
        int n_args = vec_len(&node->children);
        Node *args = node->children.buf;
        printf("  // start to call\n");
        for (int i = idx - 1; i >= 0; i--) {
            printf("  push %s\n", regs[i]);
        }
        for (int i = n_args - 1; i >= 0; i--) {
            output_expr_internal(&args[i], env, 0);
            printf("  push %s\n", regs[0]);
        }
        for (int i = 0; i < n_args; i++) {
            printf("  pop %s\n", argregs[i]);
        }
        printf("  call %s\n", node->lhs->ident);
        printf("  mov %s, rax\n", regs[idx]);
        for (int i = 0; i <= idx - 1; i++) {
            printf("  pop %s\n", regs[i]);
        }
    } else if (node->ty == PLUSPLUS_AFTER) {
        eprintf("後置++は未実装\n");
        return 1;
    } else if (node->ty == MINUSMINUS_AFTER) {
        eprintf("後置--は未実装\n");
        return 1;
    } else if (node->ty == DEREF) {
        if (node->anly_flag & LVALUE) {
            output_expr_internal(node->lhs, env, idx);
        } else {
            output_expr_internal(node->lhs, env, idx + 1);
            printf("  mov %s, [%s]\n", regs[idx], regs[idx + 1]);
        }
    } else if (node->ty == REF) {
        const Variable *var = get_variable(env, node->lhs->ident);
        if (!var) {
            eprintf("output_expr: Undeclared variable: %s\n", node->ident);
            return 1;
        }
        printf("  lea %s, [rbp - %d]\n", regs[idx], var->offset);
    } else if (0) {
    } else if (0) {
    } else if (0) {
    } else if (0) {
    } else {
        if (node->ty > 128) {
            eprintf("ごめん、未実装(output_expr_internal) %d\n", node->ty);
        } else {
            eprintf("ごめん、未実装(output_expr_internal) %c\n", node->ty);
        }
    }
    return 0;
}

static int output_expr(const Node *node, Env *env)
{
    int err;
    //printf("// expr\n");
    err = output_expr_internal(node, env, 0);
    return err;
}

static int output_stmt(const Node*, Env*);

static int output_block(const Node *node, Env *env)
{
    if (node->ty != COMPOUND_STMTS && node->ty != FUNCTION) {
        return output_stmt(node, env);
    }

    int err = 0;
    const Node *n = (const Node*)node->children.buf;
    int n_nodes = vec_len(&node->children);

    printf("// block\n");
    for (int i=0; i<n_nodes; i++) {
        err = output_stmt(&n[i], env);
        if (err) {
            eprintf("output_block: output_stmt returns err.\n");
            return err;
        }
    }
    printf("// end block\n");
    return err;
}

static int output_stmt(const Node *node, Env *env)
{
    int err = 0;
    if (node->ty == COMPOUND_STMTS) {
        err = output_block(node, env);
    } else if (node->ty == DECLVAR) {
        if (node->lhs) {
            // Assignment.
            err = output_expr(node->lhs, env);
            if (err) return err;
        }
    } else if (node->ty == RETURN) {
        Label label;
        make_label(label, env);
        err = output_expr(node->lhs, env);
        if (err) return err;
        printf("  mov rax, rsi\n");
        printf("  jmp %s\n", label);
    } else if (node->ty == IF) {
        Label label, label2;
        printf("// if stmt %p\n", node);
        err = output_expr(node->cond, env);
        if (err) return err;

        printf("  test rsi, rsi\n");
        make_label(label, node);
        printf("  jz %s\n", label);
        err = output_block(node->lhs, env);
        if (err) return err;
        if (node->rhs) {
            make_label(label2, ((const char*)node) + 1);
            printf("  jmp %s\n", label2);
            printf("  %s:\n", label);
            err = output_block(node->rhs, env);
            if (err) return err;
            printf("  %s:\n", label2);
        } else {
            printf("  %s:\n", label);
        }
        printf("// end if %p\n", node);
    } else if (node->ty == WHILE) {
        Label label_entry, label_exit;
        make_label(label_entry, node);
        make_label(label_exit, (const char*)node + 1);
        printf("// while stmt %p\n", node);
        printf("  %s:\n", label_entry);
        err = output_expr(node->cond, env);
        if (err) return err;
        printf("  test rsi, rsi\n");
        printf("  je %s\n", label_exit);
        err = output_block(node->lhs, env);
        if (err) return err;
        printf("  jmp %s\n", label_entry);
        printf("  %s:\n", label_exit);
        printf("// end while %p\n", node);
    } else if (0) {
    } else if (0) {
    } else if (0) {
    } else if (0) {
    } else if (0) {
    } else {
        err = output_expr(node, env);
    }
    return err;
}

int output_function(const Node *node)
{
    int err;
    Env *env = node->env;
    Label label;
    int n_vars = vec_len(&node->env->vars);
    Variable *vars = env->vars.buf;

    make_label(label, env);

    printf(".global %s\n", node->ident);
    printf("%s:\n", node->ident);
    printf("// prologue start\n");
    printf("  push rbp\n");
    printf("  mov rbp, rsp\n");
    for (int i=0; i<n_vars; i++) {
        if (i >= N_ARGREGS) {
            eprintf("Sorry, only %d args are available.\n", N_ARGREGS);
            return 1;
        }
        if (vars[i].varty == Arg) {
            printf("  mov [rbp-%d], %s\n", vars[i].offset, argregs[i]);
        }
        else break;
    }
    printf("  sub rsp, %d\n", n_vars * 8);
    printf("// prologue end\n");
    err = output_block(node, env);
    if (err) return err;
    printf("// epilogue start\n");
    printf("%s:\n", label);
    printf("  mov rsp, rbp\n");
    printf("  pop rbp\n");
    printf("  ret\n");
    printf("// epilogue end\n");
    return 0;
}

int output(const Node *node)
{
    int err = 0;
    puts(".intel_syntax noprefix");
    if (node->ty != GLOBAL) {
        err = output_function(node);
    } else {
        const Node *n = node->children.buf;
        int n_nodes = vec_len(&node->children);
        for (int i=0; i<n_nodes; i++) {
            err = output_function(&n[i]);
            if (err) return err;
        }
    }
    return err;
}
