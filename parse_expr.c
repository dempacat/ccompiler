#include "compiler.h"
#include "parse_internal.h"

static Node *parse_term(const Token *tokens, int *pos, Env *env)
{
    /*
    term : number
    term : "(" expr ")"
    */
    if (tokens[*pos].ty == NUM) {
        int val = tokens[*pos].val;
        (*pos)++;
        return new_node_num(val);
    }
    if (tokens[*pos].ty == SYM) {
        Node *node = new_node_ident(tokens[*pos].sym);
        (*pos)++;
        return node;
    }
    if (tokens[*pos].ty == '(') {
        (*pos)++;
        Node *node = parse_expr(tokens, pos, env);
        if (tokens[*pos].ty != ')') {
            eprintf("閉じカッコないよ\n");
            return 0;
        }
        (*pos)++;
        return node;
    }
    eprintf("parse_term(): Syntax Error\n");
    eprintf((tokens[*pos].ty < 128 ? "Token: %c\n" : "Token: %d\n"), tokens[*pos].ty);
    return 0;
}

static Node *parse_unary_left(const Token *tokens, int *pos, Env *env)
{
    Node *node = parse_term(tokens, pos, env);
    if (!node) {
        eprintf("parse_unary_left: Failed to parse term\n");
        return 0;
    }
    while (1) {
        if (tokens[*pos].ty == '(') {
            if (node->ty == SYM) {
                node = new_node(CALL, node, 0);
                (*pos)++;
                node->children = vec_new(sizeof(Node));
                while (tokens[*pos].ty != ')') {
                    Node *arg = parse_assign(tokens, pos, env);
                    vec_push(&node->children, arg);
                    if (tokens[*pos].ty == ',') (*pos)++;
                }
                (*pos)++;
            } else {
                eprintf("parse_unary_left: unknown operand called.\n");
                return 0;
            }
            continue;
        }
        if (tokens[*pos].ty == '[') {
            (*pos)++;
            Node *incl = parse_expr(tokens, pos, env);
            // node[incl] == *(node + incl)
            node = new_node(DEREF, new_node('+', node, incl), 0);
            if (tokens[*pos].ty != ']') {
                eprintf("parse_unary_left: `]` not found.\n");
                return 0;
            }
            (*pos)++;
            continue;
        }
        /* TODO: 後置++/--
        if (tokens[*pos].ty == PLUSPLUS) {
            (*pos)++;
            node = new_node('=', node, new_node('+', copy_node(node), new_node_num(1)));
            continue;
        }
        if (tokens[*pos].ty == MINUSMINUS) {
            (*pos)++;
            node = new_node('=', node, new_node('-', copy_node(node), new_node_num(1)));
            continue;
        }
        */
        // TODO: Impl dot/arrow
        break;
    }
    return node;
}

static Node *parse_unary_right(const Token *tokens, int *pos, Env *env)
{
    if (tokens[*pos].ty == PLUSPLUS) {
        (*pos)++;
        return new_node(PLUSPLUS, parse_unary_right(tokens, pos, env), 0);
    }
    if (tokens[*pos].ty == MINUSMINUS) {
        (*pos)++;
        return new_node(MINUSMINUS, parse_unary_right(tokens, pos, env), 0);
    }
    if (tokens[*pos].ty == '+') {
        (*pos)++;
        return new_node(POSSIGN, parse_unary_right(tokens, pos, env), 0);
    }
    if (tokens[*pos].ty == '-') {
        (*pos)++;
        return new_node(NEGSIGN, parse_unary_right(tokens, pos, env), 0);
    }
    if (tokens[*pos].ty == '&') {
        (*pos)++;
        return new_node(REF, parse_unary_right(tokens, pos, env), 0);
    }
    if (tokens[*pos].ty == '*') {
        (*pos)++;
        return new_node(DEREF, parse_unary_right(tokens, pos, env), 0);
    }
    // TODO: impl cast, !, ~, sizeof, _Alignof
    return parse_unary_left(tokens, pos, env);
}

#define N_OPS 5
struct ParseBinOpObject {
    enum Operand {
        BinOp,
        Unary,
        Assign,
    } operand;
    struct ParseBinOpObject *operand_obj;
    int ops[N_OPS];
};

static int match_ops(const struct ParseBinOpObject *obj, int ty)
{
    for (int i=0; i<N_OPS; i++) {
        if (ty == obj->ops[i]) {
            return ty;
        }
        if (!obj->ops[i]) {
            break;
        }
    }
    return 0;
}
#undef N_OPS

struct ParseBinOpObject mul = { Unary, 0, {'*', '/', '%'}};
struct ParseBinOpObject add = { BinOp, &mul, {'+', '-'}};
struct ParseBinOpObject shift = { BinOp, &add, {LSHIFT, RSHIFT}};
struct ParseBinOpObject cmp = { BinOp, &shift, {'<', '>', LE, GE}};
struct ParseBinOpObject eq = { BinOp, &cmp, {EQ, NE}};
struct ParseBinOpObject bitand = { BinOp, &eq, {'&'}};  // asm出力は未実装
struct ParseBinOpObject bitxor = { BinOp, &bitand, {'^'}};  // asm出力は未実装
struct ParseBinOpObject bitor = { BinOp, &bitxor, {'|'}};  // asm出力は未実装
struct ParseBinOpObject logicand = { BinOp, &bitor, {ANDAND}};  // asm出力は未実装
struct ParseBinOpObject logicor = { BinOp, &logicand, {OROR}};  // asm出力は未実装
// TODO: Impl 三項演算子
struct ParseBinOpObject comma = { Assign, 0, {','}};

static Node *parse_binop(const struct ParseBinOpObject *obj, const Token *tokens, int *pos, Env *env)
{
    // SubExpr : Operand SubExpr'
    // SubExpr': ε | ops[0] SubExpr | ops[1] SubExpr | ...
    Node *lhs;
    if (obj->operand == BinOp) {
        lhs = parse_binop(obj->operand_obj, tokens, pos, env);
    } else if (obj->operand == Unary) {
        lhs = parse_unary_right(tokens, pos, env);
    } else if (obj->operand == Assign) {
        lhs = parse_assign(tokens, pos, env);
    } else {
        eprintf("parse_binop: Unknown operand type\n");
        return 0;
    }
    if (!lhs) {
        eprintf("parse_binop: Failed to parse LHS\n");
        return 0;
    }
    int ty = tokens[*pos].ty;
    while (match_ops(obj, ty)) {
        Node *rhs;
        (*pos)++;
        if (obj->operand == BinOp) {
            rhs = parse_binop(obj->operand_obj, tokens, pos, env);
        } else if (obj->operand == Unary) {
            rhs = parse_unary_right(tokens, pos, env);
        } else if (obj->operand == Assign) {
            rhs = parse_assign(tokens, pos, env);
        } else {
            eprintf("parse_binop: Unreachable!!\n");
            return 0;
        }
        if (!rhs) {
            eprintf("parse_binop: Failed to parse RHS\n");
            return 0;
        }
        lhs = new_node(ty, lhs, rhs);
        ty = tokens[*pos].ty;
    }
    return lhs;
}

Node *parse_assign(const Token *tokens, int *pos, Env *env)
{
    Node *lhs = parse_binop(&logicor, tokens, pos, env), *rhs;
    int ty = tokens[*pos].ty;
    int table[] = {
        ADDASSIGN, '+',
        SUBASSIGN, '-',
        MULASSIGN, '*',
        DIVASSIGN, '/',
        MODASSIGN, '%',
        LSHIFTASSIGN, LSHIFT,
        RSHIFTASSIGN, RSHIFT,
        ANDASSIGN, '&',
        ORASSIGN, '|',
        XORASSIGN, '^',
        0, 0 };
    if (!lhs) {
        eprintf("parse_assign: Failed to parse LHS\n");
        return 0;
    }
    if (ty == '=') {
        (*pos)++;
        rhs = parse_assign(tokens, pos, env);
        if (!rhs) {
            eprintf("parse_assign: Failed to parse LHS\n");
        }
        return new_node('=', lhs, rhs);
    }
    for (int i = 0; table[i] != 0; i += 2) {
        if (ty == table[i]) {
            (*pos)++;
            rhs = parse_assign(tokens, pos, env);
            return new_node('=', lhs, new_node(table[i + 1], copy_node(lhs), rhs));
        }
    }
    return lhs;
}

Node *parse_expr(const Token *tokens, int *pos, Env *env)
{
    return parse_binop(&comma, tokens, pos, env);
}
