#!/bin/bash
run() {
    input="$1"

    if [ "$2" = "-g" ]; then
        gdb -args ./fcc "$input" -d 
        return 0
    fi
    ./fcc "$input" -d > tmp.s
    if [ "$2" = "-G" ]; then
        cat -n tmp.s
        echo ""
        gcc -g -o tmp tmp.s
        gdb ./tmp
        return 0
    fi
    if [ "$?" -eq "0" ]; then
        cat -n tmp.s
        echo ""
        gcc -o tmp tmp.s
        ./tmp
        actual="$?"
        echo "Return: $actual"
    else
        echo "Compile failed"
        cat -n tmp.s
        return 1
    fi
}

make && run "$1" "$2" && rm tmp tmp.s
