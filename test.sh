#!/bin/bash
try() {
    expected="$1"
    input="$2"

    ./fcc "$input" > tmp.s
    gcc -o tmp tmp.s
    ./tmp
    actual="$?"

    if [ "$actual" = "$expected" ]; then
        echo "$input => $actual"
    else
        echo "$input => $expected expected, but got $actual"
        exit 1
    fi
}

try 0 "{return 0;}"
try 42 "{123; return 42; return 45;52;}"
try 6 "return 1+2+3;"
try 0 "return 3-2-1;"
try 6 "  return    1	+ 1+1     +1 +1+1;"
try 6 "return    1	+ 1+1     +1 +1+1   ;  "
try 24 "return 2*3*4;"
try 26 "return 2*3+4*5;"
try 2 "return 8/4;"
try 2 "return 9/4;"
try 3 "return 15%6;"
try 0 "return 15%5;"
try 0 "return (2<1)+(3>4);"
try 2 "return (2>1)+(3<4);"
try 2 "return (2>1)*2+(3>4);"
try 1 "return 2*(2<2)+(2<=2)+4*(2<=1);"
try 6 "return 2*(3>=3)+(3>3)+4*(4>=3);"
try 5 "return (1==1)+2*(2==3)+4*(1!=2)+8*(2!=2);"
try 2 "return (2>1)*2+(3>4);"
try 20 "return 5<<2;"
try 15 "return 255>>4;"
try 2 "return ((((1))))+((((1))));"
try 4 "return ((((2))))*((((2))));"
try 10 "return (2+3)*(4-2);"
try 2 "{if (10-2*5) { return 1; } if (3) { return 2; } return 3;}"
try 2 "{if (10-2*5) return 1; if (3) return 2; return 3;}"
try 3 "{if (10-2*5) return 1; if (3) 1+2; return 3;}"
try 4 "{ if (0) { if (1) return 1; return 2; } if (2) { if (0) return 3; return 4; } return 5;}"
try 2 "{if (10-2*5) return 1; else return 2;}"
try 2 "{if (10-2*5) return 1; else if (1) return 2; else return 3;}"
try 3 "{if (10-2*5) return 1; else if (0) return 2; else return 3;}"
try 3 "/*  */ { 1; /* return 2; */ return 3; /*foo*/} /**/"
try 3 "// { return 2; }{{{
           { 1; // return 2;
           return 3;}//"
try 2 "{int a; a = 1; a = 2; return a;}"
try 5 "{int a; a = 1; a = 2 + 3; return a;}"
try 30 "{int a; a = 1; a = a + 4; return a + a * a;}"
try 7 "{int b, a; b = 3; a = 4; b = a + b; a = b + 1; return b;}"
try 8 "{int a, b; b = 3; a = 4; b = a + b; a = b + 1; return a;}"
try 12 "{int asdfasdf; int asdf; asdf = 3; asdfasdf = 4; return asdf * asdfasdf;}"
try 6 "{int asdfasdf; int asdf; asdf = 3; asdfasdf = asdf + asdf; return asdfasdf;}"
try 21 "{ return 1+(2+(3+(4+(5+6))));}"
try 55 "{ int acc; int i; acc = 0; i = 10; while (i) { acc = acc + i; i = i - 1; } return acc; }"
try 35 "{ int a; a = 2; a += 5 - 2; a *= 7; return a; }"
try 2 "{ int nyan; nyan = 5; nyan -= 7 - 4; return nyan; }"
try 22 "{ int a, b; a = b = 2; return a * 10 + b; }"
try 74 "{ int a, b; a = b = 3; a += b = 4; return a*10+b; }"
try 77 "{ int a, b; a = b = 3; a = b += 4; return a*10+b; }"
try 3 "{int a; return a = 3;}"
try 10 "{return 3, 4+5, 2*5;}"
try 5 "{int a; return 4, a=2, 23*45, a+3; }"
try 12 "{int a = 3, b = a+1; return a * b;}"
try 46 "{int a = 3, b = a = 4, c = (b, 2); return a*10 + b + c;}"
try 6 "int two(){return 2;}int three(void){return 3;}int main(){return two() * three();}"
try 9 "int two(){return 2;}int three(void){return 3;}int main(){int a = two() * three(); return three() + a;}"
try 15 "int add(int a, int b){ return a+b; } int main() { return add(1, add(2, 3)) + add(4, add(0, 5)); }"
try 13 "int f(int a, int b) { return a*2+b; } int main() { return f(5, 3); }"
try 9 "int f(int a, int b, int c, int d, int e, int f) { return 0; } int main() { return 2 + 3 + 4 + f(5, 6, 7, 8, 9, 10); }"
try 15 "int main() { int a = 1, b = 3; ++a; ++b; return ++a * ++b; }"
try 0 "int main() { return +1 + -1  - +2 + (-2) + +4; }"
try 3 "int main() { int a = 1; int *p = &a; *p = 3; return a; }"
try 3 "int main() { int a = 1; int *p = &a; *p = 3; return *p; }"
try 3 "int main() { int a = 1; int *p = &a, **pp = &p; **pp = 3; return a; }"
try 3 "int main() { int a = 1; int *p = &a, **pp = &p; **pp = 3; return **pp; }"
try 9 "int main() { int a = 1, *p = &a, **pp = &p; **pp = 3; return a + *p + **pp; }"
try 1 "int main() { int a = 1, *p = &a, *q = (p + 0); return *q; }"
try 1 "int main() { int a = 1, *p = &a, *q = (p + 0); return *(q+0); }"
try 2 "int main() { int a = 1, *p = &a; a = 2; return p[0]; }"
try 2 "int main() { int a = 1, *p = &a; p[0] = 2; return a; }"
try 2 "int main() { int a = 1, *p = &a; p[0] = 2; return p[0]; }"
try 3 "/*Undefined Behavior!*/int main() {int a; int b; int c; int *p = &b; p[1] = 1; p[0] = 2; return p[0] + p[1];}"
try 6 "int main() { int a[3]; a[0] = 1; a[1] = 2; a[2] = 3; return a[0] + a[1] + a[2]; }"
try 2 "int main() { int a = 2, *p[1]; p[0] = &a; *p[0] = 3; return *p[0]; }"
try 6 "int main() { int a = 2, b; int *p[1]; p[0] = &a; b = *p[0]; *p[0] = 3; return a * b; }"
try 120 "int main() { int a = 2, b = 3, c = 5, *p[3]; p[0] = &b; p[1] = &c; p[2] = &a; return *p[0] * *p[0] + *p[1] + *p[2] * *p[2] * *p[2]; }"

# Shouldn't pass, but passed.
try 0 "/*Shouldn't Pass!*/int main() {if(1) { int a=0; } return a;}"
try 0 "/*Shouldn't Pass!*/int f(void){return 0;}int main() {return f(1,2,3);}"
try 0 "/*Shouldn't Pass!*/int f(int a){return 0;}int main() {return f();}"
try 1 "/*Shouldn't Pass!*/int main ( ) { int a ; int a ; return 1 ; }"


# Not passed
# try 28 "{ return 1+(2+(3+(4+(5+(6+7)))));}" # register spill
# try 9 "int f(int a, int b, int c, int d, int e, int f, int g) { return 0; } int main() { return f(1, 2, 3, 4, 5, 6, 7); }" # Too many args

rm tmp tmp.s
echo OK
