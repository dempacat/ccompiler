#include "compiler.h"
#include "parse_internal.h"

#define RET_IF(expr) { int t = expr; if (t) { eprintf("RET_IF failed\n"); return t; } }

int analyze_block(Node* node, Env* env);

int analyze_expr(Node *node, Env *env, int is_lvalue)
{
    if (is_lvalue) node->anly_flag |= LVALUE;
    if (is_binary_op(node->ty)) {
        if (node->ty == '=') {
            RET_IF(analyze_expr(node->lhs, env, 1));
            RET_IF(analyze_expr(node->rhs, env, 0));
            //if (!(node->lhs->anly_flag & LVALUE)) {
            //    eprintf("assignment operator's lhs must be a lvalue.\n");
            //    return 1;
            //}
            node->rettype = node->lhs->rettype;
            return 0;
        }
        RET_IF(analyze_expr(node->lhs, env, 0));
        RET_IF(analyze_expr(node->rhs, env, 0));
        if (node->ty == '+') {
            Type *lhstype = &node->lhs->rettype;
            Type *rhstype = &node->rhs->rettype;
            if (lhstype->ty == Int && rhstype->ty == Int) {
                node->rettype.ty = Int;
                node->rettype.ptrof = 0;
            } else if ((lhstype->ty == Ptr && rhstype->ty == Int) ||
                       (lhstype->ty == Int && rhstype->ty == Ptr)) {
                if (lhstype->ty == Int) {
                    // Swap lhs and rhs.
                    Node *tmp = node->lhs;
                    node->lhs = node->rhs;
                    node->rhs = tmp;
                }
                node->rettype = node->lhs->rettype;
                node->anly_flag |= PTR_ADDSUB;
            } else if ((lhstype->ty == Array && rhstype->ty == Int) ||
                       (lhstype->ty == Int && rhstype->ty == Array)) {
                if (lhstype->ty == Int) {
                    // Swap lhs and rhs.
                    Node *tmp = node->lhs;
                    node->lhs = node->rhs;
                    node->rhs = tmp;
                }
                Node *tmp = node->lhs;
                node->lhs = new_node(REF, tmp, 0);
                node->anly_flag |= PTR_ADDSUB;
                node->rettype = node->lhs->rettype;
            } else {
                eprintf("analyze_expr: type error for add.\n");
                return 1;
            }
        } else if (node->ty == '-') {
            Type *lhstype = &node->lhs->rettype;
            Type *rhstype = &node->rhs->rettype;
            if (lhstype->ty == Int && rhstype->ty == Int) {
                node->rettype.ty = Int;
                node->rettype.ptrof = 0;
            } else if (lhstype->ty == Ptr && rhstype->ty == Ptr) {
                node->rettype.ty = Int;
                node->rettype.ptrof = 0;
                node->anly_flag |= PTR_DIFF;
            } else if (lhstype->ty == Ptr && rhstype->ty == Int) {
                node->rettype.ty = Ptr;
                node->rettype.ptrof = malloc(sizeof(Type));
                node->rettype.ptrof->ty = Int;
                node->rettype.ptrof->ptrof = 0;
                node->anly_flag |= PTR_ADDSUB;
            } else {
                eprintf("analyze_expr: type error for sub.\n");
                return 1;
            }
        } else if (node->ty == ',') {
            node->rettype = node->rhs->rettype;
        // } else if (node->ty == '<') {
        // } else if (node->ty == '>') {
        // } else if (node->ty == LE) {
        // } else if (node->ty == GE) {
        // } else if (node->ty == EQ) {
        // } else if (node->ty == NE) {
        } else {
            Type *lhstype = &node->lhs->rettype;
            Type *rhstype = &node->rhs->rettype;
            if (lhstype->ty == Int && rhstype->ty == Int) {
                node->rettype.ty = Int;
                node->rettype.ptrof = 0;
            } else {
                if (node->ty < 128) {
                    eprintf("analyze_expr: type error for binop %c.\n", node->ty);
                } else {
                    eprintf("analyze_expr: type error for binop %d.\n", node->ty);
                }
                return 1;
            }
        }
    } else if (node->ty == NUM) {
        node->rettype.ty = Int;
        node->rettype.ptrof = 0;
    } else if (node->ty == SYM) {
        //node->anly_flag |= LVALUE;
        const Variable *v = get_variable(env, node->ident);
        node->rettype = v->ty;
    } else if (node->ty == CALL) {
        node->rettype.ty = Int;
        node->rettype.ptrof = 0;
    } else if (node->ty == PLUSPLUS) {
        Node *lhs = node->lhs;
        Node *alter = new_node('=', lhs, new_node('+', copy_node(lhs), new_node_num(1)));
        *node = *alter;
        free(alter);
        RET_IF(analyze_expr(node, env, 0));
    } else if (node->ty == MINUSMINUS) {
        Node *lhs = node->lhs;
        Node *alter = new_node('=', lhs, new_node('-', copy_node(lhs), new_node_num(1)));
        *node = *alter;
        free(alter);
        RET_IF(analyze_expr(node, env, 0));
    } else if (node->ty == POSSIGN) {
        Node *lhs = node->lhs;
        *node = *lhs;
        free(lhs);
        RET_IF(analyze_expr(node, env, 0));
        if (node->rettype.ty != Int) {
            eprintf("analyze_expr: type error for positive sign. Type must be an arithmetic type.\n");
            return 1;
        }
    } else if (node->ty == NEGSIGN) {
        Node *lhs = node->lhs;
        Node *alter = new_node('-', new_node_num(0), lhs);
        *node = *alter;
        free(alter);
        RET_IF(analyze_expr(node, env, 0));
        if (node->rettype.ty != Int) {
            eprintf("analyze_expr: type error for positive sign. Type must be an arithmetic type.\n");
            return 1;
        }
    } else if (node->ty == REF) {
        RET_IF(analyze_expr(node->lhs, env, 1));
        if (node->lhs->ty == DEREF) {
            // (REF (DEREF (EXPR))) to (EXPR).
            Node *tmp = node->lhs;
            *node = *node->lhs->lhs;
            free(tmp->lhs);
            free(tmp);
        } else if (node->lhs->ty != SYM) {
            node->rettype.ty = Ptr;
            node->rettype.ptrof = &node->lhs->rettype;
            eprintf("analyze_expr: Error for ref. Operand must be a symbol, *(expr) or array[idx].\n");
            return 1;
        }
        //if (!(node->lhs->anly_flag & LVALUE)) {
        //    eprintf("analyze_expr: Error for ref. Operand must be a lvalue.\n");
        //    return 1;
        //}
    } else if (node->ty == DEREF) {
        RET_IF(analyze_expr(node->lhs, env, 0));
        if (node->lhs->rettype.ty != Ptr) {
            eprintf("analyze_expr: type error for deref. Type must be a pointer type (%d) but type %d.\n", Ptr, node->lhs->rettype.ty);
            return 1;
        }
        node->rettype = *node->lhs->rettype.ptrof;
        //node->anly_flag |= LVALUE;
    } else if (node->ty == -1) {
    } else if (node->ty == -1) {
    } else if (node->ty == -1) {
    } else {
        if (node->ty < 128) {
            eprintf("analyze_expr: Unknown node %c\n", node->ty);
        } else {
            eprintf("analyze_expr: Unknown node %d\n", node->ty);
        }
        return 1;
    }
    return 0;
}

int analyze_stmt(Node *node, Env *env)
{
    if (node->ty == COMPOUND_STMTS) {
        return analyze_block(node, env);
    } else if (node->ty == DECLVAR) {
        if (node->lhs) return analyze_expr(node->lhs, env, 0);
        return 0;
    } else if (node->ty == RETURN) {
        return analyze_expr(node->lhs, env, 0);
    } else if (node->ty == IF) {
        int err;

        err = analyze_expr(node->cond, env, 0);
        if (err) return err;
        err = analyze_stmt(node->lhs, env);
        if (err) return err;
        if (node->rhs) {
            return analyze_stmt(node->rhs, env);
        }
        return 0;
    } else if (node->ty == WHILE) {
        int err;

        err = analyze_expr(node->cond, env, 0);
        if (err) return err;
        return analyze_stmt(node->lhs, env);
    } else if (0) {
    } else if (0) {
    } else if (0) {
    } else if (0) {
    } else if (0) {
    } else {
        return analyze_expr(node, env, 0);
    }
    eprintf("unknown path\n");
    return 1;
}

int analyze_block(Node *node, Env *env)
{
    if (node->ty != COMPOUND_STMTS && node->ty != FUNCTION) {
        return analyze_stmt(node, env);
    }
    int err = 0;
    Node *n = (Node*)node->children.buf;
    int n_nodes = vec_len(&node->children);
    for (int i = 0; i < n_nodes; i++) {
        err = analyze_stmt(&n[i], env);
        if (err) return err;
    }
    return 0;
}

int analyze_function(Node *node)
{
    /*
    int n_vars = vec_len(&node->env->vars);

    for (int i = 0; i < n_vars; i++) {
    }
    */
    return analyze_block(node, node->env);
}

int analyze(Node *node)
{
    if (node->ty != GLOBAL) {
        return analyze_function(node);
    } else {
        Node *n = node->children.buf;
        int n_nodes = vec_len(&node->children);
        for (int i=0; i<n_nodes; i++) {
            int err = analyze_function(&n[i]);
            if (err) return err;
        }
        return 0;
    }
}
