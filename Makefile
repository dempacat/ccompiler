CC=gcc
# fcc means my first c compiler.
fcc: compiler.c compiler.h parse.c parse_expr.c parse_nodealloc.c parse_internal.h output.c utility.c analyze.c
	$(CC) -g -Wall -Wextra -o $@ $^

test: fcc
	./test.sh
