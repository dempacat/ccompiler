#include "compiler.h"
#include <stdio.h>
#include <stdlib.h>

Env *allocate_env(void)
{
    Env *env = malloc(sizeof(Env));
    env->vars = vec_new(sizeof(Variable));
    env->stack_size = 0;
    return env;
}

const Variable *get_variable(const Env *env, const char *name)
{
    int len = vec_len(&env->vars);
    for (int i=0; i<len; i++) {
        const Variable *var = ((const Variable*)env->vars.buf) + i;
        if (!my_strcmp(var->name, name)) return var;
    }
    return 0;
}

void dump_type(const Type *ty)
{
    switch(ty->ty) {
    case Void:
        eprintf("VOID\n");
        break;
    case Int:
        eprintf("INT\n");
        break;
    case Ptr:
        eprintf("* ");
        dump_type(ty->ptrof);
        break;
    case Array:
        eprintf("[%d] ", ty->arrsize);
        dump_type(ty->ptrof);
        break;
    }
}

int get_varsize(Env *env, Type ty)
{
    switch (ty.ty) {
    case Void:
        eprintf("get_varsize: Size of void is refered. It may bug!.\n");
        return 0;
    case Int:
        return 8;
    case Ptr:
        return 8;
    case Array:
        // TODO: Array of array?
        return 8 * ty.arrsize;
    }
    eprintf("get_varsize: Unexpected type.\n");
    return 8;
}

int decl_variable(Env *env, const char *name, Type ty, int vartype)
{
    if (get_variable(env, name) != 0) {
        eprintf("Variable `%s` is already decleared.\n", name);
        return 1;
    }
    int size = get_varsize(env, ty);
    int offset = env->stack_size + size;
    Variable var = { name, offset, ty, vartype };
    vec_push(&env->vars, &var);
    env->stack_size += size;
    return 0;
}

typedef struct {
    int val;
    int length;
} GetNum;

GetNum getnum(const char *buf)
{
    int val = 0, length = 0;
    while ('0' <= *buf && *buf <= '9') {
        val = 10 * val + (*buf - '0');
        length++;
        buf++;
    }
    GetNum ret = {val, length};
    return ret;
}

int getsym(const char *buf)
{
    if (*buf == '_' || ('A' <= *buf && *buf <= 'Z') || ('a' <= *buf && *buf <= 'z')) {
        int length = 1;
        buf++;
        while (*buf == '_' ||
                ('A' <= *buf && *buf <= 'Z') ||
                ('a' <= *buf && *buf <= 'z') ||
                ('0' <= *buf && *buf <= '9')) {
            buf++;
            length += 1;
        }
        return length;
    }
    return 0;
}

#define N_SECONDS 4
struct Tokenizer2CharObject {
    char first_ch;
    char second_chs[N_SECONDS];
    int toks[N_SECONDS];
};

int match_token(const struct Tokenizer2CharObject *obj, Token **ptokens, const char *buf)
{
    if (buf[0] != obj->first_ch) {
        return 0;
    }
    if (!buf[1]) {
        (*ptokens)->ty = *buf;
        (*ptokens)++;
        return 1;
    }
    for (int i=0; i<N_SECONDS; i++) {
        if (buf[1] == obj->second_chs[i]) {
            (*ptokens)->ty = obj->toks[i];
            (*ptokens)++;
            return 2;
        }
    }
    (*ptokens)->ty = *buf;
    (*ptokens)++;
    return 1;
}

struct Tokenizer2CharObject twochar_tok[] = {
    {'+', {'+', '='}, {PLUSPLUS, ADDASSIGN}},
    {'-', {'-', '=', '>'}, {MINUSMINUS, SUBASSIGN, ARROW}},
    {'*', {'='}, {MULASSIGN}},
    {'%', {'='}, {MODASSIGN}},
    {'!', {'='}, {NE}},
    {'=', {'='}, {EQ}},
    {'&', {'&', '='}, {ANDAND, ANDASSIGN}},
    {'|', {'|', '='}, {OROR, ORASSIGN}},
    {'^', {'='}, {XORASSIGN}}
};

// binary op, but not handled by twochar_tok: /
// 3char: >, >>, >>=, >=, <, <<, <<=, <=

int is_binary_op(int ty)
{
    for (int i = 0; i < (int)sizeof(twochar_tok) / (int)sizeof(struct Tokenizer2CharObject); i++) {
        if (ty == twochar_tok[i].first_ch) {
            return ty;
        }
        for (int j=0; j<N_SECONDS; j++) {
            if (ty == twochar_tok[i].toks[j] && ty != PLUSPLUS && ty != MINUSMINUS && ty != PLUSPLUS_AFTER && ty != MINUSMINUS_AFTER) {
                return ty;
            }
        }
    }
    if (ty == '/' || ty == '>' || ty == RSHIFT || ty == RSHIFTASSIGN || ty == GE ||
                     ty == '<' || ty == LSHIFT || ty == LSHIFTASSIGN || ty == LE || ty == ',') {
        return ty;
    }
    return 0;
}

#undef N_SECONDS

int tokenize(Token *tokens, const char *buf)
{
    while (*buf) {
        if ('0' <= *buf && *buf <= '9') {
            GetNum num = getnum(buf);
            buf += num.length;
            tokens->ty = NUM;
            tokens->val = num.val;
            tokens++;
        } else if (
            *buf == ';' || *buf == ',' || *buf == '~' ||
            *buf == '(' || *buf == ')' || *buf == '{' || *buf == '}' ||
            *buf == '[' || *buf == ']') {
            tokens->ty = *buf;
            tokens++;
            buf++;
        } else if (*buf == '/') {
            if (buf[1] == '/') {
                // Comment
                int i = 2;
                while (buf[i] != '\n' && buf[i] != 0) i++;
                if (buf[i]) i++;
                buf += i;
            } else if (buf[1] == '*') {
                /* Comment */
                int i = 2;
                while (!(buf[i] == '*' && buf[i+1] == '/') && buf[i] != 0) {
                    i++;
                }
                if (buf[i]) i += 2;
                buf += i;
            } else {
                // Divide
                tokens->ty = *buf;
                tokens++;
                buf++;
            }
        } else if (*buf == '<') {
            // < or <= or << or <<=
            if (buf[1] == '<') {
                if (buf[2] == '=') {
                    tokens->ty = LSHIFTASSIGN;
                    tokens++;
                    buf += 3;
                } else {
                    tokens->ty = LSHIFT;
                    tokens++;
                    buf += 2;
                }
            } else if (buf[1] == '=') {
                tokens->ty = LE;
                tokens++;
                buf += 2;
            } else {
                tokens->ty = '<';
                tokens++;
                buf++;
            }
        } else if (*buf == '>') {
            // > or >= or >> or >>=
            if (buf[1] == '>') {
                if (buf[2] == '=') {
                    tokens->ty = RSHIFTASSIGN;
                    tokens++;
                    buf += 3;
                } else {
                    tokens->ty = RSHIFT;
                    tokens++;
                    buf += 2;
                }
            } else if (buf[1] == '=') {
                tokens->ty = GE;
                tokens++;
                buf += 2;
            } else {
                tokens->ty = '>';
                tokens++;
                buf++;
            }
        } else if (*buf == ' ' || *buf == '\t') {
            buf++;
        } else if (*buf == '\n') {
            buf++;
        } else {
            int length = 0;
            for (int i=0; i<(int)sizeof(twochar_tok)/(int)sizeof(struct Tokenizer2CharObject); i++) {
                length = match_token(&twochar_tok[i], &tokens, buf);
                if (length) {
                    break;
                }
            }
            if (length) {
                buf += length;
                continue;
            }
            // ↑ ここまで記号のトークン化
            // ↓ ここからキーワードとシンボル
            length = getsym(buf);
            if (length == 0) {
                fprintf(stderr, "Unexpected charactor `%c`(0x%02x)\n", *buf, *buf);
                return 1;
            }
            if (length == 6 && !my_strncmp(buf, "return", 6)) {
                tokens->ty = RETURN;
                tokens++;
                buf += length;
            } else if (length == 3 && !my_strncmp(buf, "int", 3)) {
                tokens->ty = INT;
                tokens++;
                buf += length;
            } else if (length == 2 && !my_strncmp(buf, "if", 2)) {
                tokens->ty = IF;
                tokens++;
                buf += length;
            } else if (length == 4 && !my_strncmp(buf, "else", 4)) {
                tokens->ty = ELSE;
                tokens++;
                buf += length;
            } else if (length == 5 && !my_strncmp(buf, "while", 5)) {
                tokens->ty = WHILE;
                tokens++;
                buf += length;
            } else if (length == 3 && !my_strncmp(buf, "for", 3)) {
                tokens->ty = FOR;
                tokens++;
                buf += length;
                eprintf("for stmt may not unimplemented.\n");
                return 1;
            } else if (length == 4 && !my_strncmp(buf, "void", 4)) {
                tokens->ty = VOID;
                tokens++;
                buf += length;
            } else if (0) {
            } else if (0) {
            } else if (0) {
            } else if (0) {
            } else {
                tokens->ty = SYM;
                tokens->sym = alloc_str(buf, length);
                tokens++;
                buf += length;
            }
        }
    }
    tokens->ty = FILE_END;
    return 0;
}

void dump_node_rec(const Node *node, int depth, int show_anly)
{
    for (int i=0; i<depth; i++) eprintf("  ");
    if (node->ty == NUM) {
        eprintf("NUM %d\n", node->val);
    }
    else if (is_binary_op(node->ty)) {
        if (node->ty < 128) {
            eprintf("BIN OP %c ", node->ty);
        } else if (node->ty == GE) {
            eprintf("BIN OP >= ");
        } else if (node->ty == LE) {
            eprintf("BIN OP >= ");
        } else if (node->ty == EQ) {
            eprintf("BIN OP == ");
        } else if (node->ty == NE) {
            eprintf("BIN OP != ");
        } else {
            eprintf("BIN OP (%d) ", node->ty);
        }
        if (show_anly) {
            eprintf("anly: %x\n", node->anly_flag);
        } else {
            eprintf("\n");
        }
        dump_node_rec(node->lhs, depth + 1, show_anly);
        dump_node_rec(node->rhs, depth + 1, show_anly);
    } else if (node->ty == FUNCTION) {
        const Node *n = (const Node*)node->children.buf;
        const Variable *vars = (const Variable*)node->env->vars.buf;
        int n_nodes = vec_len(&node->children);
        int n_vars = vec_len(&node->env->vars);
        eprintf("Function %s\n", node->ident);
        for (int i=0; i<depth+1; i++) eprintf("  ");
        eprintf("Variables: ");
        for (int i=0; i<n_vars; i++) {
            eprintf("%s (%s) ", vars[i].name, vars[i].varty == Arg ? "arg" : "local");
        }
        eprintf("\n");
        for (int i=0; i<depth+1; i++) eprintf("  ");
        eprintf("Statements (len: %d)\n", n_nodes);
        for (int i=0; i<n_nodes; i++) dump_node_rec(&n[i], depth + 2, show_anly);
    } else if (node->ty == RETURN) {
        eprintf("RETURN\n");
        dump_node_rec(node->lhs, depth + 1, show_anly);
    } else if (node->ty == COMPOUND_STMTS) {
        const Node *n = (const Node*)node->children.buf;
        int n_nodes = vec_len(&node->children);
        eprintf("COMPOUND STMTS (len: %d)\n", n_nodes);
        for (int i=0; i<n_nodes; i++) dump_node_rec(&n[i], depth + 1, show_anly);
    } else if (node->ty == IF) {
        eprintf("IF\n");
        dump_node_rec(node->cond, depth + 1, show_anly);
        dump_node_rec(node->lhs, depth + 1, show_anly);
        if (node->rhs, show_anly) dump_node_rec(node->rhs, depth + 1, show_anly);
    } else if (node->ty == WHILE) {
        eprintf("WHILE\n");
        dump_node_rec(node->cond, depth + 1, show_anly);
        dump_node_rec(node->lhs, depth + 1, show_anly);
    } else if (node->ty == SYM) {
        if (show_anly) {
            eprintf("SYM %s anly: %x ", node->ident, node->anly_flag);
            dump_type(&node->rettype);
        } else {
            eprintf("SYM %s\n", node->ident);
        }
    } else if (node->ty == DECLVAR) {
        eprintf("DECLVAR\n");
        if (node->lhs) dump_node_rec(node->lhs, depth + 1, show_anly);
    } else if (node->ty == GLOBAL) {
        const Node *n = (const Node*)node->children.buf;
        int n_nodes = vec_len(&node->children);
        eprintf("GLOBAL (%d nodes)\n", n_nodes);
        for (int i=0; i<n_nodes; i++) dump_node_rec(&n[i], depth, show_anly);
    } else if (node->ty == CALL) {
        const Node *n = (const Node*)node->children.buf;
        int n_nodes = vec_len(&node->children);
        eprintf("CALL %s\n", node->lhs->ident);
        for (int i=0; i<n_nodes; i++) dump_node_rec(&n[i], depth + 1, show_anly);
    } else if (node->ty == PLUSPLUS) {
        if (show_anly) {
            eprintf("UNARY OP ++ (Prefix) anly: %x ", node->anly_flag);
            dump_type(&node->rettype);
        } else {
            eprintf("UNARY OP ++ (Prefix)\n");
        }
        dump_node_rec(node->lhs, depth + 1, show_anly);
    } else if (node->ty == MINUSMINUS) {
        if (show_anly) {
            eprintf("UNARY OP -- (Prefix) anly: %x ", node->anly_flag);
            dump_type(&node->rettype);
        } else {
            eprintf("UNARY OP -- (Prefix)\n");
        }
        dump_node_rec(node->lhs, depth + 1, show_anly);
    } else if (node->ty == REF) {
        if (show_anly) {
            eprintf("UNARY OP & anly: %x ", node->anly_flag);
            dump_type(&node->rettype);
        } else {
            eprintf("UNARY OP &\n");
        }
        dump_node_rec(node->lhs, depth + 1, show_anly);
    } else if (node->ty == DEREF) {
        if (show_anly) {
            eprintf("UNARY OP * anly: %x ", node->anly_flag);
            dump_type(&node->rettype);
        } else {
            eprintf("UNARY OP *\n");
        }
        dump_node_rec(node->lhs, depth + 1, show_anly);
    } else if (0) {
    } else if (0) {
    } else {
        if (node->ty < 128) eprintf("Unknown: %c\n", node->ty);
        else eprintf("Unknown: %d\n", node->ty);
    }
}

void dump_token(const Token *tokens)
{
    const Token *tok_start = tokens;
    eprintf("dump_token\nTokens: ");
    eprintf("\n");
    while (tokens->ty != FILE_END) {
        if (tokens->ty == NUM) eprintf("%d ", tokens->val);
        else if (tokens->ty == RETURN) eprintf("return ");
        else if (tokens->ty == IF) eprintf("if ");
        else if (tokens->ty == ELSE) eprintf("else ");
        else if (tokens->ty == WHILE) eprintf("while ");
        else if (tokens->ty == FOR) eprintf("for ");
        else if (tokens->ty == INT) eprintf("int ");
        else if (tokens->ty == VOID) eprintf("void ");
        else if (tokens->ty == SYM) eprintf("%s ", tokens->sym);
        else if (tokens->ty == PLUSPLUS) eprintf("++ ");
        else if (tokens->ty == MINUSMINUS) eprintf("++ ");
        else if (tokens->ty == REF) eprintf("& ");
        else if (tokens->ty == DEREF) eprintf("* ");
        else if (tokens->ty < 127) eprintf("%c ", tokens->ty);
        else if (tokens->ty == ADDASSIGN) eprintf("+= ");
        else if (tokens->ty == SUBASSIGN) eprintf("-= ");
        else if (tokens->ty == MULASSIGN) eprintf("*= ");
        else if (tokens->ty == DIVASSIGN) eprintf("/= ");
        else if (tokens->ty == MODASSIGN) eprintf("%= ");
        else if (tokens->ty == LSHIFTASSIGN) eprintf("<<= ");
        else if (tokens->ty == RSHIFTASSIGN) eprintf(">>= ");
        else if (tokens->ty == ANDASSIGN) eprintf("&= ");
        else if (tokens->ty == XORASSIGN) eprintf("^= ");
        else if (tokens->ty == ORASSIGN) eprintf("|= ");
        else { eprintf("unknown(%d) ", tokens->ty); }
        tokens++;
    }
    eprintf("\n");
    eprintf("Number of tokens: %ld\n", tokens - tok_start);
    eprintf("dump_token done.\n");
}

void dump_node(Node *node, int show_anly)
{
    eprintf("dump_node\nNodes:\n");
    dump_node_rec(node, 0, show_anly);
    eprintf("dump_node done.\n");
}

int main(int argc, char** argv)
{
    Token tokens[1000];
    Node *node;
    int dump_flg = 0;

    if (argc < 2) return 1;
    if (tokenize(tokens, argv[1]) != 0) {
        return 1;
    }
    if (argc >= 3 && argv[2][1] == 'd') {
        dump_flg = 1;
    }
    if (dump_flg) dump_token(tokens);
    node = parse(tokens);
    if (!node) {
        eprintf("Compile failed (node is null)\n");
        return 1;
    }
    if (dump_flg) dump_node(node, 0);
    if (analyze(node)) {
        eprintf("Compile failed (analyze error)\n");
        if (dump_flg) dump_node(node, 1);
        return 1;
    }
    if (dump_flg) eprintf("Analyze done.\n");
    output(node);
    if (dump_flg) eprintf("Output done.\n");
    return 0;
}
