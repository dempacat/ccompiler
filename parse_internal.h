#ifndef PARSE_H
#define PARSE_H

#include "compiler.h"

Node *new_node(int ty, Node *lhs, Node *rhs);
Node *new_node_num(int val);
Node *new_node_compound(int ty, Vec v);
Node *new_node_conditional(int ty, Node *cond, Node *lhs, Node *rhs);
Node *new_node_ident(const char *name);
Node *new_node_function(const char *name);

Node *copy_node(const Node *node);

Node *parse_assign(const Token *tokens, int *pos, Env *env);
Node *parse_expr(const Token *tokens, int *pos, Env *env);

#endif
