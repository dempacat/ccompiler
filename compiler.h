#ifndef COMPILER_H
#define COMPILER_H

#include <stdio.h>
#include <stdlib.h>

typedef char Label[18];

typedef enum {
    LSHIFT = 1000,
    RSHIFT,
    GE, // >=
    LE, // <=
    EQ, // ==
    NE, // !=
    DEREF, // Unary * operator for Node.
    REF, // Unary & operator for Node.
    POSSIGN, // Unary + operator for Node.
    NEGSIGN, // Unary - operator for Node.
    SIZEOF,
    CAST, // Type cast for Node.
    ANDAND,
    OROR,
    DOT,
    ARROW,
    PLUSPLUS, // ++ token, or prefix increment for Node
    PLUSPLUS_AFTER, // postfix increment for Node
    MINUSMINUS, // -- token, or prefix decrement for Node
    MINUSMINUS_AFTER, // postfix decrement for Node
    CALL,
    ADDASSIGN,
    SUBASSIGN,
    MULASSIGN,
    DIVASSIGN,
    MODASSIGN,
    LSHIFTASSIGN,
    RSHIFTASSIGN,
    ANDASSIGN,
    XORASSIGN,
    ORASSIGN,
    INT = 2000,
    VOID,
    IF,
    ELSE,
    FOR,
    WHILE,
    RETURN,
    NUM = 3000,
    SYM,
    COMPOUND_STMTS,
    DECLVAR,
    EXPR,
    FUNCTION,
    GLOBAL,
    FILE_END = 9999,
} TokenType;

typedef struct {
    int ty;
    int val;
    char *sym;
} Token;

typedef struct {
    void *buf;
    int type_size;
    int cap;
    int ofs;
} Vec;

typedef struct Env {
    Vec vars;
    int stack_size;
} Env;

enum VarType {
    Local = 1,
    Arg,
};

enum TypeId {
    Void,
    Int,
    Ptr,
    Array,
};

typedef struct Type {
    int ty;
    int arrsize;
    struct Type *ptrof;
} Type;

typedef struct Variable {
    const char *name;
    int offset;
    Type ty;
    int varty;
} Variable;

enum AnalyzeFlag {
    LVALUE = 0x10000,
    PTR_ADDSUB = 0x20000,
    PTR_DIFF = 0x40000,
};

typedef struct Node {
    int ty;
    Type rettype;
    struct Node *lhs;
    struct Node *rhs;
    struct Node *cond;
    Vec children;
    int val;
    unsigned int anly_flag;
    const char *ident;
    Env *env;
} Node;

#define eprintf(...) fprintf(stderr, __VA_ARGS__)

Node *parse(const Token *tokens);

Vec vec_with_cap(int type_size, int cap_len);
Vec vec_new(int type_size);
void vec_free(Vec *vec);
void vec_push(Vec *vec, const void *elem);
int vec_len(const Vec *vec);

int my_strncmp(const char *a, const char *b, int n);
int my_strcmp(const char *a, const char *b);
void my_strncpy(char *dest, const char *src, int n);

char *alloc_str(const char *str, int len);

int is_binary_op(int ty);
int output(const Node *node);

Env *allocate_env(void);

int decl_variable(Env *env, const char *name, Type ty, int vartype);
const Variable *get_variable(const Env *env, const char *name);

int analyze(Node *node);

#endif
