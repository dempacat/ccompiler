#include "compiler.h"
#include <stdlib.h>

int my_strncmp(const char *a, const char *b, int n)
{
    while (n--) {
        if (*a - *b) return *a - *b;
        a++; b++;
    }
    return 0;
}

int my_strcmp(const char *a, const char *b)
{
    while (*a == *b && *a && *b) {
        a++; b++;
    }
    return *a - *b;
}

void my_strncpy(char *dest, const char *src, int n)
{
    while (n--) {
        *dest++ = *src++;
    }
}

char *alloc_str(const char *str, int len)
{
    char *p = malloc(len + 1);
    my_strncpy(p, str, len);
    p[len] = 0;
    return p;
}

Vec vec_with_cap(int type_size, int cap_len)
{
    void *buf = malloc(type_size * cap_len);
    Vec vec = { buf, type_size, type_size * cap_len, 0 };
    return vec;
}

Vec vec_new(int type_size)
{
    Vec vec = { malloc(type_size), type_size, type_size, 0 };
    return vec;
}

void vec_free(Vec *vec)
{
    free(vec->buf);
    vec->cap = vec->ofs = 0;
}

void vec_push(Vec *vec, const void *elem)
{
    if (vec->ofs + vec->type_size >= vec->cap) {
        void *newbuf = malloc(vec->cap * 2);
        my_strncpy((char*)newbuf, (char*)vec->buf, vec->cap);
        free(vec->buf);
        vec->buf = newbuf;
        vec->cap *= 2;
    }
    my_strncpy(vec->buf + vec->ofs, elem, vec->type_size);
    vec->ofs += vec->type_size;
}

int vec_len(const Vec *vec)
{
    return vec->ofs / vec->type_size;
}

